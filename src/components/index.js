import React, { Component } from 'react';
import List from './List';
import Update from './update';


class Index extends Component {

    getText = () => {
        let text = this.refs.text.value.trim();
        if (text !== "") {
            this.props.getText(text);
            this.refs.text.value = "";
        }
        else {
            alert("khong được bỏ trống ");
        }
    }
    deleteItem = (index) => {
        this.props.deleteItem(index);
    }
    updateText = (text) => {
        this.refs.text.value = text;

    }
    setUpdate = () => {
        this.props.setUpdate();
    }
    getItem = (id,text) => {
        this.props.getItem(id,text);
    }
    checkItem = (obj) => {
        if (obj === true) {

            return (
                <Update
                    atcUpdate ={this.props.atcUpdate}
                    update ={this.props.Update}
                />
            )
        }
        else {
            return (
                <div>
                    <h2>thêm công việc</h2>
                    <div className="row">
                        <div className="col">
                            <div className="form-group">
                                Nhập Name: <input type="text" ref="text" className="form-control" />
                            </div>
                            <div>
                                <button type="submit"
                                    onClick={this.getText}
                                    className="btn btn-primary m-2">them</button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
    }
    render() {

        let element = this.props.List.map((e, i) => {
            return (
                <React.Fragment key={i}>
                    <List
                        item={e}
                        index={i}
                        updateText={this.updateText}
                        deleteItem={this.deleteItem}
                        setUpdate={this.setUpdate}
                        getItem={this.getItem}
                    />
                </React.Fragment>
            );
        });
        return (
            <div className="container mt-5 pt-2 border" style={{ background: "aliceblue" }} >


                {this.checkItem(this.props.isToggerForm)}
                {element}
                <hr />

            </div>
        );
    }
}

export default Index;
