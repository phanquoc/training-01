import React, { Component } from 'react';



class List extends Component {

    onDelete = () => {
        this.props.deleteItem(this.props.index);
    }
    setUpdate = () => {
        let text = this.props.item;
        let id = this.props.index;
        this.props.getItem(id,text);
        this.props.setUpdate();
    }
    render() {

        return (
            <div>
                <div className="form-group">
                    <ul className="list-group">
                        <li className="list-group-item" onClick={this.updateText}>{this.props.item}
                            <div className="float-right">
                                <button
                                    type="button"
                                    className="btn btn-default "
                                    onClick={this.onDelete}
                                >xoa
                    </button>
                                <button
                                    type="button"
                                    className="btn btn-success ml-3"
                                    onClick={this.setUpdate}
                                >update
                    </button>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}



export default List;
