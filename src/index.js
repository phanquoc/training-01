import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Index from './container/index'
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import AppReducer from './reducer/index';
import thunk from 'redux-thunk';

let store = createStore(AppReducer, applyMiddleware(thunk));
class App extends React.Component {
    render() {
        return (
            <Index />
        )
    }
}
ReactDOM.render(
    <Provider store={store}>

        <App />
    </Provider>
    , document.getElementById('root'));
