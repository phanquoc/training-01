import { ADD } from './../constants/type';
export const getText = (text) => {
    return {
        type: ADD.ACTION_ADD,
        text
    }
};

export const getQuoc = () => {
    return {
        type: ADD.ACTION_QUOC,
        typeQuoc: "quoc"
    }
};
export const deleteItem = (index) => {
    return {
        type: ADD.ACTION_DELETE,
        index 
    }
};
export const setUpdate = () => {
    return {
        type: ADD.ISTOGGER_FROM,
    }
};
export const getItem = (id,text) => {
    return {
        type: ADD.ACTION_GET_TEXT,
        id,
        text,
    }
};
export const update = (id,text) => {
    return {
        type: ADD.ACTION_UPDATE,
        id,
        text,
    }
};