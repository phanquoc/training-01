import { combineReducers } from 'redux';
import StateApp from './stateApp';
import isToggerForm from './isToggerForm';
import Update from './update';
const AppReducer = combineReducers({
    StateApp,
    isToggerForm,
    Update

});
export default AppReducer;
