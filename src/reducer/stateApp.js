import { ADD } from '../constants/type';

var initialState = ["quoc","trinh"
];

const StateApp = (state = initialState, action) => {
    switch (action.type) {
        case ADD.ACTION_ADD:
            state.push(action.text)
            return [...state];

        case ADD.ACTION_QUOC:
            state.push(action.typeQuoc);
            return [...state];

        case ADD.ACTION_DELETE:
            state.splice(action.index, 1);
            return [...state];
        case ADD.ACTION_UPDATE:

            state[action.id] = action.text;
            return [...state];

        default:
            return state;
    }
}
export default StateApp;