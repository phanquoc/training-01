import { ADD } from '../constants/type';

var initialState = false;

const isToggerForm = (state = initialState, action) => {
    switch (action.type) {
        case ADD.ISTOGGER_FROM:
            state = !state;
            return state;
        default:
            return state;
    }
}
export default isToggerForm;