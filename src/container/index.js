import React, { Component } from 'react';
import Index from './../components/index';
import * as Act from './../action/add';
import { connect } from 'react-redux';

class IndexContainer extends Component {
    getText = (text) => {
        this.props.getText(text);
    }
    getQuoc = () => {
        this.props.getQuoc();
    }
    deleteItem = (index) => {
        this.props.deleteItem(index);
    }
    setUpdate =()=>{
        this.props.setUpdate();
    }
    getItem = (id,text) =>{
        this.props.getItem(id,text);
    }
    atcUpdate = (id,text) =>{
        this.props.update(id,text);
    }
    render() {
        return (
            <div>
                <Index       
                    getText ={this.getText}
                    List ={this.props.List}
                    deleteItem={this.deleteItem}
                    isToggerForm ={this.props.isToggerForm}
                    setUpdate={this.setUpdate}
                    getItem ={this.getItem}
                    Update ={this.props.Update}
                    atcUpdate ={this.atcUpdate}
                    
                />
            </div>
        );
    }
}
const mapStateToProps = (state) => {
    return {
        List: state.StateApp,
        isToggerForm: state.isToggerForm,
        Update : state.Update

    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getText: (text) => {
            dispatch(Act.getText(text));
        },
        getQuoc: () => {
            dispatch(Act.getQuoc());
        },
        deleteItem: (index) => {
            dispatch(Act.deleteItem(index));
        },
        setUpdate: () => {
            dispatch(Act.setUpdate());
        },
        getItem: (id,text) => {
            dispatch(Act.getItem(id,text));
        },
        update: (id,text) => {
            dispatch(Act.update(id,text));
        }
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(IndexContainer);